%global srcname  ligo-segments

Name:           %{srcname}
Version:        1.4.0
Release:        2.1%{?dist}
Summary:        Representations of semi-open intervals

License:        GPLv3
URL:            https://git.ligo.org/lscsoft/ligo-segments/
Source0:        https://software.igwn.org/sources/source/%{srcname}-%{version}.tar.gz

# macros
BuildRequires:  python-rpm-macros
%if 0%{?rhel} > 0 && 0%{?rhel} < 8
BuildRequires:  python2-rpm-macros
%endif
BuildRequires:  python3-rpm-macros

# build
BuildRequires:  gcc
%if 0%{?rhel} > 0 && 0%{?rhel} < 8
BuildRequires:  python-devel
BuildRequires:  python2-setuptools
BuildRequires:  python-six
%endif
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-six

%description
This module defines the segment and segmentlist objects, as well as the
infinity object used to define semi-infinite and infinite segments.

%if 0%{?rhel} > 0 && 0%{?rhel} < 8
%package -n python2-%{srcname}
Summary:  %{summary}
Requires: python2-six
Requires: python2-ligo-common
# not supported by old rpmbuild
#Recommends: python2-lal

%{?python_provide:%python_provide python2-%{srcname}}

%description -n python2-%{srcname}
This module defines the segment and segmentlist objects, as well as the
infinity object used to define semi-infinite and infinite segments.
%endif

%package -n python%{python3_pkgversion}-%{srcname}
Summary:  %{summary}
Requires: python%{python3_pkgversion}-six
# not supported by old rpmbuild
#Recommends: python3-lal

%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}

%description -n python%{python3_pkgversion}-%{srcname}
This module defines the segment and segmentlist objects, as well as the
infinity object used to define semi-infinite and infinite segments.

%prep
%autosetup -n %{srcname}-%{version}

%build
%if 0%{?rhel} > 0 && 0%{?rhel} < 8
%py2_build
%endif
%py3_build

%install
%if 0%{?rhel} > 0 && 0%{?rhel} < 8
%py2_install
%endif
%py3_install

%check
%if 0%{?rhel} > 0 && 0%{?rhel} < 8
PYTHONPATH="%{buildroot}%{python2_sitearch}" \
%{__python2} -c "from ligo.segments import (segment, segmentlist, segmentlistdict)"
%endif
PYTHONPATH="%{buildroot}%{python3_sitearch}" \
%{__python3} -c "from ligo.segments import (segment, segmentlist, segmentlistdict)"

%if 0%{?rhel} > 0 && 0%{?rhel} < 8
%files -n python2-%{srcname}
%license LICENSE
%doc README.rst
%{python2_sitearch}/*
%exclude %{python2_sitearch}/ligo/__init__.py*
%endif

%files -n python%{python3_pkgversion}-%{srcname}
%license LICENSE
%doc README.rst
%{python3_sitearch}/*
%exclude %{python3_sitearch}/ligo/__init__.py
%exclude %{python3_sitearch}/ligo/__pycache__/

%changelog
* Thu Mar 30 2023 Michael Thomas <michael.thomas@ligo.org> - 1.4.0-2.1
- Remove more python2 references on RHEL >= 9

* Thu Apr 21 2022 Duncan Macleod <duncan.macleod@ligo.org> - 1.4.0-2
- only build python2 packages on rhel < 8
- exclude top-level namespace __init__.py files
- update source location
- run make check during check phase

* Thu May 10 2018 Duncan Macleod <duncan.macleod@ligo.org>
- 1.0.0: first release of ligo.segments, should be funtionally identical to glue.segments
